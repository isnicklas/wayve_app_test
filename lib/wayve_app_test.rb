require 'wayve_app_test/version'
require 'wayve_app_test/wayve_test_one'
require 'wayve_app_test/wayve_test_two'
require 'wayve_app_test/wayve_test_three'

module WayveAppTest
  class Application
    def initialize
      puts 'Please select one of the tests by entering 1 2 or 3
===============================================================
1. palindrome
2. longest_string
3. The pyramid'

      @test_number = gets.chomp.to_i

      self.test_selector
    end

    def test_selector
      return 'Wrong Choice' if test_number < 1 || test_number > 3

      case test_number
        when 1
          test_case_one
        when 2
          test_case_two
        else
          puts "Great, this is test 3. Enter the following;
                WayveTestThree::Application.maximum_sum_path('path_to_ur_file or enter example.txt')"
      end
    end

    private

    def test_case_one
      test1 = WayveTestOne::Application.new

      loop do
        word = gets.chomp

        break if word == 'exit-test-one'

        test1.palindrome?(word)
        puts 'Enter a new word'
      end
    end

    def test_case_two
      test2 = WayveTestTwo::Application.new

      loop do
        hash_entry = eval(gets.chomp)
        break if hash_entry == 'exit-test-two'
        test2.longest_string(hash_entry)

        puts 'Enter A New Hash'
      end
    end

    attr_reader :test_number
  end
end

module WayveTestOne
  class Application
    def initialize
      puts "Great! Test One Selected. This is a palindrome detector.
Enter a word and I will tell you if it is a palindrome.
If you have had enough enter 'exit-test-one' to quit\n\n"
    end

    def palindrome?(word)
      @word = word

      return puts 'Not a valid entry' if word.size < 2 || word =~ /(\s)/

      word.size.even? ? even_palindrome : odd_palindrome
    end

    private

    def even_palindrome
      characters   = word.split("")
      middle       = (word.size / 2)
      set_one      = characters[0..(middle - 1)]
      set_two      = []
      two_unsorted = characters[middle.. -1]

      set_two << two_unsorted.pop until two_unsorted.empty?

      p palindrome = set_one == set_two ? true : false

      palindrome
    end

    def odd_palindrome
      characters   = word.split("")
      middle       = (word.size / 2)
      set_one      = characters[0..(middle)]
      set_two      = []
      two_unsorted = characters[middle.. -1]

      set_two << two_unsorted.pop until two_unsorted.empty?

      p palindrome = set_one == set_two ? true : false

      palindrome
    end

    attr_reader :word
  end
end
module WayveTestThree
  class Application

    def self.maximum_sum_path(path = File.join(Dir.pwd, '/example.text'))
      total   = 0
      c_index = 0

      File.open(path, 'r').each_line do |line|
        numbers = line.split("").reject! { |num| num == " " || num == "\n" }

        if numbers.size == 1
          puts line
          next total += numbers[0].to_i
        end
        new_numbers = numbers[c_index..(c_index + 1)]
        max_number  = new_numbers.max
        c_index     = new_numbers.index(new_numbers.max)

        total += max_number.to_i

        puts line
      end

      puts total

      total
    end
  end
end
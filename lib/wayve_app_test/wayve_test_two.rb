module WayveTestTwo
  class Application
    def initialize
      puts "Great, this is test 2. Enter a key - value pair
  { foo: 'bar', this: 'baaaaaaar' }
 and i will tell you the longest word in the hash.
Enter exit-test-2 when you've had enough \n \n"
    end

    def longest_string(payload = {})
      longest_word = " "

      payload.each_pair do |_ , value|
        longest_word = value if longest_word.size < value.size
      end

      puts "longest word in this hash is #{longest_word}"

      longest_word
    end
  end
end
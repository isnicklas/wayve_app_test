require 'spec_helper'

describe WayveTestOne::Application do
  subject { described_class.new }

  context 'when input is invalid' do
    context 'when user enters just a letter' do
      it "return 'not a valid entry'" do
        just_a_letter = 'a'

        puts subject.palindrome?(just_a_letter)

        expect { subject.palindrome?(just_a_letter) }
          .to output("Not a valid entry\n").to_stdout
      end
    end

    context 'when user enters two words with spacing' do
      it "return 'not a valid entry'" do
        just_a_letter = 'aab baa'

        puts subject.palindrome?(just_a_letter)

        expect { subject.palindrome?(just_a_letter) }
          .to output("Not a valid entry\n").to_stdout
      end
    end
  end

  context 'when input is valid' do
    context 'when user enters a palindrome with EVEN number size' do
      it "returns true" do
        just_a_letter = 'aabbaa'

        expect(subject.palindrome?(just_a_letter)).to be true
      end
    end

    context 'when word is not a palindrome with EVEN number size' do
      it "returns false" do
        just_a_letter = 'aabbca'

        expect(subject.palindrome?(just_a_letter)).to be false
      end
    end

    context 'when user enters a palindrome with ODD number size' do
      it "returns true" do
        just_a_letter = 'asa'

        expect(subject.palindrome?(just_a_letter)).to be true
      end
    end

    context 'when word is not a palindrome with ODD number size' do
      it "returns false" do
        just_a_letter = 'assca'

        expect(subject.palindrome?(just_a_letter)).to be false
      end
    end
  end
end
require 'spec_helper'

describe WayveTestTwo::Application do
  subject { described_class.new }

  describe '#longest_string' do
    let(:hash_input) { { foo: 'bar', this: 'baaaaaaar' } }

    it 'prints baaaaaaar' do
      expect(subject.longest_string(hash_input))
        .to eq("baaaaaaar")
    end
  end
end
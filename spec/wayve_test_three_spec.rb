require 'spec_helper'

describe WayveTestThree::Application do
  let(:path) { File.join(Dir.pwd, '/example.txt') }
  it 'returns a total of 24' do
    expect(described_class.maximum_sum_path(path)).to be 24
  end
end